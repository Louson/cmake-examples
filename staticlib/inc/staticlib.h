#ifndef _STATICLIB_H_
#define _STATICLIB_H_

void static_init(void);
void static_func(void);
void static_term(void);

#endif  // _STATICLIB_H_
