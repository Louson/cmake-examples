#include "sharedlib.h"

#include <stdio.h>

void shared_init(void) {
  printf("Sharedlib: %s\n", __func__);
}

void shared_func(void) {
  printf("Sharedlib: %s\n", __func__);
}

void shared_term(void) {
  printf("Sharedlib: %s\n", __func__);
}
