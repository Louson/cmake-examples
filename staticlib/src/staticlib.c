#include "staticlib.h"

#include <stdio.h>

void static_init(void) {
  printf("Staticlib: %s\n", __func__);
}

void static_func(void) {
  printf("Staticlib: %s\n", __func__);
}

void static_term(void) {
  printf("Staticlib: %s\n", __func__);
}
